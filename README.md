#### Twitter Sentiment Analysis

## Requirement
- Tweepy
- TextBlob

## How to run
- Create a twitter API
- Open Terminal
- Go inside the project folder
- Set consumer_key, consumer_secret, access_token, access_token_secret and search_keyword
- Input the following command in the terminal

    ``python test.py``
